import java.sql.*;

public class Main {
	public static void main(String[] args) {
		try {
			Connection connection =
				DriverManager.getConnection(

					"jdbc:postgresql://localhost:5432/postgres",
				//	"jdbc:postgresql:pro3_db",
					"postgres", "student_1234");
			Statement statement = connection.createStatement();
			ResultSet result = statement.executeQuery("SELECT * FROM EMPLOYEES");
			while (result.next()){
				System.out.println(result.getInt("id")
					+ " " + result.getString("firstname")
				+ " " + result.getInt("salary"));
			}
			result.close();
			statement.close();
			connection.close();

		} catch (SQLException e) {
			System.err.println("DB error: " );
			e.printStackTrace();
		}
	}
}